#+title: Convert Time in Region to Local Timezone
#+OPTIONS: toc:nil

[[../index.org][index]]

#+begin_verse
There is no time,
but we have timezones.
                        [unknown zen apprentice]
#+end_verse

:catchwords:
[2021-01-27 Wed] {convenience time timezones}
:END:

I want something in Emacs to convert a time string containing a
timezone to the local timezone on my machine.

Possibly program src_text[:exports code]{date} can do this.  ISTR that the fsf page
proposed src_text[:exports code]{date} to do such conversion in the context of a
worldwide conference so the participants can easily check the time in
their local timezone.

Let's check some of src_text[:exports code]{date}'s abilities.

#+begin_src shell
$ date --date='18:30 january 27, 2021 PST'
Thu Jan 28 03:30:00 CET 2021
#+end_src

Nice, but I don't want the timezone specifier.  I don't need it.  It's
been converted in my normal time.  I don't want that extra text.

#+begin_src shell
$ date --date='18:30 january 27, 2021 PST' +"%Y-%m-%d %H:%M"
2021-01-28 03:30
#+end_src

Great!  LGTM.

So let's rely on src_text[:exports code]{date} for the hard work.

* Functionality :code:

#+begin_src emacs-lisp
(defun mw-region-time-in-local-tz (start end)
  "Interpret region START END as time. Show time in local timezone.

Example(-!- is point, -m- is mark):

given:

    -!-18:30 january 27, 2021 PST-m-

action:

{ M-x mw-region-time-in-local-tz RET }

result (note: the local zone is CET):

    see the message '2021-01-28 03:30' in the minibuffer.
"
  (interactive "r")
  (message
   "%s"
   (shell-command-to-string
    (concat
     "date --date='" (buffer-substring-no-properties start end) "'"
     " +'%Y-%m-%d %H:%M'"))))
#+end_src

#+INCLUDE: "../meta.org"
